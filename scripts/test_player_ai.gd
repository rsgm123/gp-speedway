extends Node

var xDir = 0
var a = false
var b = false


func _process(delta):
    if Input.is_action_just_pressed("ui_left"):
        xDir = -1
        
    if Input.is_action_just_released("ui_left"):
        xDir = 0
        
    if Input.is_action_just_pressed("ui_right"):
        xDir = 1
        
    if Input.is_action_just_released("ui_right"):
        xDir = 0

    if Input.is_action_just_pressed("ui_up"):
        a = true
        
    if Input.is_action_just_released("ui_up"):
        a = false

    if Input.is_action_just_pressed("ui_down"):
        b = true
        
    if Input.is_action_just_released("ui_down"):
        b = false
