extends PanelContainer


const track_scenes = 'res://scenes/tracks/%s.tscn'
const SPLITSCREEN_SCENE = preload('res://scenes/splitscreen.tscn')

onready var tracks = $MarginContainer/CenterContainer/tracks

var selection = 0


func _ready():
    Network.connect('input', self, '_input_handler')


func _input_handler(player, content):
    var action = content['data']['action']
    var key = content['data']['key']
    
    if action == 'keyUp':
        tracks.get_child(selection).get_node('select').visible = false
        
        if key == 'left' and selection % tracks.columns > 0: 
            selection -= 1
        elif key == 'right' and (selection % tracks.columns < tracks.columns - 1 and selection < tracks.get_child_count() - 1):
            selection += 1
            
        elif key == 'up' and selection > tracks.columns - 1: 
            selection -= tracks.columns
        elif key == 'down' and selection < tracks.get_child_count() - 1 - tracks.columns:
            selection += tracks.columns
        
        elif key == 'enter':
            _select_track()
        
        tracks.get_child(selection).get_node('select').visible = true        


func _select_track():
    var track = tracks.get_child(selection).name
    
    Game.track_scene = track_scenes % track
    get_tree().change_scene_to(SPLITSCREEN_SCENE)
