extends Node2D


const RESULTS_SCENE = preload('res://scenes/race_results.tscn')

onready var track_path = $track_path.get_curve()
onready var vehicles = $vehicles
onready var starting_positions = $starting_positions

var total_track_length

var started = false
var laps = 2
var finished_vehicles = 0


func _ready():
    Network.send('setController', {
        'vertical': false,
        'modules': [
            {'module': 'ThumbStick'},
            {'module': 'ButtonFour'},
    ]})
    
    total_track_length = track_path.get_baked_length()
    
    for e in Utils.enumerate(Network.players.values()):
        e.object.data.lap_times = []
        
        var vehicle = load(Game.vehicle_scene).instance()
        vehicle.name = str(e.index)
        vehicle.track = self
        vehicle.player = e.object
        vehicle.initialize(e.object.data.color)
        
        var starting_position = starting_positions.get_child(e.index)
        vehicle.position = starting_position.position
        vehicle.rotation += starting_position.rotation
        vehicles.add_child(vehicle)
    
#    yield(get_tree().create_timer(3), 'timeout')
    started = true


func _physics_process(delta):
    var player_distances = []
    for vehicle in vehicles.get_children():
        if vehicle.lap != null:
            var distance = track_path.get_closest_offset(vehicle.position) + vehicle.lap * total_track_length
            player_distances.append([int(vehicle.name), distance])

    player_distances.sort_custom(Utils, 'second_element_sort')
    player_distances.invert()
    for e in Utils.enumerate(player_distances):
        vehicles.get_child(e.object[0]).race_position = e.index + 1


func player_finished():
    finished_vehicles += 1
    if finished_vehicles >= vehicles.get_child_count():
        # go to results screen after 5 seconds
        print('race finished')
        
        # play finish animation
        yield(get_tree().create_timer(5), 'timeout')
        get_tree().change_scene_to(RESULTS_SCENE)
