extends KinematicBody2D


const PLAYER_AI = preload('res://scripts/player_ai.gd')

export (Script) var devmode_ai

var top_speed = 0
var max_turning = 0
var max_acceleration = 0
var max_deceleration = 0
var max_reverse = 0  # figure out a nice way to stop to 0 then reverse

var acceleration_curve
var turning_curve

var speed = 0

var track
var race_position
var lap
var race_finished = false
var lap_time
var lap_checkpoint = false

var player
var ai


func _ready():
    print(devmode_ai)
    if devmode_ai:  # for devmode testing of specific track scenes
        track = get_node('../../')
        ai = devmode_ai.new()
        add_child(ai)


func initialize(color):
    $color_stripe.modulate = Color(Utils.PLAYER_COLORS[color])
    ai = PLAYER_AI.new()
    add_child(ai)


func _physics_process(delta):
    if race_finished:
        decelerate(delta)
        
    elif ai and track.started: # and game started
        if ai.xDir != 0:
            turn(delta, ai.xDir)
            
        if ai.a:
            accelerate(delta)
        if ai.b:
            decelerate(delta)
    
    var velocity = Vector2.UP.rotated(rotation) * speed
    var collision = move_and_collide(velocity * delta)
    if collision:
        var other_body = collision.collider
        
         # calculate the new velocities of both vehicles
        if other_body.is_in_group('vehicle'):
            velocity = velocity.bounce(collision.normal)
            rotation = velocity.angle()
            speed = velocity.length()
            
            var other_velocity = Vector2.UP.rotated(other_body.rotation) * other_body.speed
            other_velocity = other_velocity.bounce(collision.normal)
            other_body.rotation = other_velocity.angle()
            other_body.speed = other_velocity.length()
            
        # just bounce off
        else:
            velocity = velocity.bounce(collision.normal)
            rotation = velocity.angle()
            speed *= 0.5


func turn(delta, direction):
    if speed > 0:
        var change = _lerp_curve(turning_curve, speed/top_speed) * max_turning
        rotation += change * direction * delta
        print(change * direction * delta)

func accelerate(delta):
    if speed < top_speed:
        var change = _lerp_curve(acceleration_curve, speed/top_speed) * max_acceleration
        speed += change * delta
    else:
        speed = top_speed

func decelerate(delta):
    if speed > 0:
        speed -= max_deceleration * delta
    else:
        speed = 0


func _lerp_curve(map, value):
    """
    Finds the two elements in the array in which the value falls.
    This will then take the lerp of the values.
    Used for finding acceleration and turning for given speeds.
    """
    var a
    var b
    for i in range(map.size()):
        if value <= map[i][0]:
            a = map[i-1]
            b = map[i]
            break
    
    if not a or not b:
        printerr('values not found')
    
    var weight = (value - a[0]) / (b[0] - a[0])
    return lerp(a[1], b[1], weight)


func lap_finished():
    if lap == null:
        lap = 0
        lap_time = OS.get_system_time_msecs()
    
    # make sure the player is on the last track segment
    # (points - 1 is the last point, but we measure from the start of the segment)
    elif lap_checkpoint:
        lap_checkpoint = false
        lap += 1
        player.data.lap_times.append(OS.get_system_time_msecs() - lap_time)
        lap_time = OS.get_system_time_msecs()
        
        if lap == track.laps:
            race_finished = true
            track.player_finished()
            print('player %s finished' % name)
