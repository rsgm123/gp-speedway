extends 'res://scripts/vehicles/vehicle_base.gd'


func _ready():
    top_speed = 600
    max_turning = 3.5
    max_acceleration = 120
    max_deceleration = 160
    
    acceleration_curve = [
        [0, 0.4],
        [0.2, 1],
        [0.3, 1],
        [0.5, 0.6],
        [0.8, 0.2],
        [1, 0],
    ]
    turning_curve = [
        [0, 0],
        [0.05, 0.6],
        [0.1, 0.4],
        [0.3, 1],
        [0.6, 0.9],
        [0.8, 0.6],
        [1, 0.5],
    ]

