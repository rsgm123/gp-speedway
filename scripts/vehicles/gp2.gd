extends 'res://scripts/vehicles/vehicle_base.gd'


func _ready():
    top_speed = 400
    max_turning = 3
    max_acceleration = 60
    max_deceleration = 100
    
    acceleration_curve = [
        [0, 1],
        [0.2, 1],
        [0.5, 0.8],
        [0.7, 0.2],
        [0.9, 0.1],
        [1, 0],
    ]
    turning_curve = [
        [0, 0],
        [0.05, 1],
        [0.1, 0.6],
        [0.8, 0.7],
        [1, .5],
    ]
