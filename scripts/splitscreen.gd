extends Node


const SCREEN_SCENE = preload('res://scenes/components/splitscreen_player.tscn')

onready var viewports = get_node('viewports')

func _ready():
    var player_count = Network.players.size()
    viewports.columns = ceil(sqrt(player_count))
    # todo: change camera zoom depending on player_count
    # todo: move hud to different sides depending on player count
    
    var player_0 = SCREEN_SCENE.instance()
    var player_0_viewport = player_0.get_node('vc/viewport')
    player_0.name = 0
    viewports.add_child(player_0)
    
    
    for i in range(player_count - 1):
        var viewport = player_0.duplicate()
        viewport.name = str(i + 1)
        viewports.add_child(viewport)
    
    var world = load(Game.track_scene).instance()
    player_0_viewport.add_child(world)
    var world_2d = player_0_viewport.world_2d # scene is converted into a world2d object available on viewport
    
    for i in range(player_count):
        viewports.get_node('%d/vc/viewport' % i).world_2d = world_2d
        
        var vehicle = world.get_node('vehicles/%d' % i)
        viewports.get_node('%d/vc/viewport/camera' % i).target = vehicle
        viewports.get_node('%d/hud/speed_guage' % i).target = vehicle
        viewports.get_node('%d/hud/position_guage' % i).target = vehicle
        
    $minimap_container/minimap.initialize(world)
