extends PanelContainer


const vehicle_scenes = 'res://scenes/vehicles/%s.tscn'

const TRACK_SELECT = preload('res://scenes/track_select.tscn')
onready var vehicles = $MarginContainer/CenterContainer/vehicles

var selection = 1


func _ready():
    Network.connect('input', self, '_input_handler')
    Network.send('setController', {
        'vertical': false,
        'modules': [
            {'module': 'DPadOne'},
    ]})
    
    


func _input_handler(player, content):
    var action = content['data']['action']
    var key = content['data']['key']
    
    if action == 'keyUp':
        vehicles.get_child(selection).get_node('select').visible = false
        
        if key == 'left' and selection > 1: # 0:
            selection -= 1
        elif key == 'right' and selection < 2: #vehicles.get_child_count() - 1:
            selection += 1
            
        elif key == 'enter':
            _select_vehicle()
        
        vehicles.get_child(selection).get_node('select').visible = true


func _select_vehicle():
    var vehicle = vehicles.get_child(selection).name
    
    Game.vehicle_scene = vehicle_scenes % vehicle
    get_tree().change_scene_to(TRACK_SELECT)
