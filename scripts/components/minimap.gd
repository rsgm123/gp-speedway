extends Node


onready var blips = $world/blips
onready var track = $world/track

var vehicles = []


func initialize(world):
    vehicles = world.get_node('vehicles').get_children()
    
    track.texture = world.get_node('track').texture
    
    var blip_0 = blips.get_child(0)
    for e in Utils.enumerate(vehicles):
        if e.index > 0:
            var blip = blip_0.duplicate()
            blip.name = str(e.index + 1)
            blip.set_modulate(e.object.get_modulate())
            blips.add_child(blip)


func _physics_process(delta):
    if vehicles:
        for e in Utils.enumerate(vehicles):
            var blip = blips.get_child(e.index)
            blip.position = e.object.position
