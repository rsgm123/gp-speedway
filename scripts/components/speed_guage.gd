extends Control


const MAX_ROTATION = 3 * PI / 2
onready var needle = $guage/needle

var target

func _physics_process(delta):
    if target:
        var speed = target.speed / target.top_speed
        needle.rotation = speed * MAX_ROTATION

    else:
        needle.rotation = 0
