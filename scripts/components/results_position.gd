extends PanelContainer



var place
var best_time
var total_time
var player_data
var top_best_time


func _ready():
    $hbox/place.text = str(place)
    $hbox/PanelContainer/name.text = player_data.name
    # color ?
    $hbox/best_time.text = Utils.timestamp(best_time)
    $hbox/total_time.text = Utils.timestamp(total_time)
    
    if top_best_time:
        $hbox/best_time_star.text = '*'
