extends VBoxContainer


var target

func _physics_process(delta):
    if target and target.race_position != null:
        $position/number.text = str(target.race_position)
        $lap/lap.text = str(target.lap)
        $lap/total_laps.text = str(target.track.laps)
