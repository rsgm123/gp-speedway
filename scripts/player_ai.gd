extends Node


var xDir = 0
var a = false
var b = false


func _ready():
    print('connected ai to signal')
    get_parent().player.connect('input', self, '_input_handler')

# this will need a 2d-axis controller, and support for that input

func _input_handler(p, content):
    var action = content['data']['action']
    var key = content['data']['key']
    
    if key == 'stick2d':
        xDir = content['data']['point']['x']
            
    if key == 'a':
        if action == 'keyDown':
            a = true
        if action == 'keyUp':
            a = false
            
    if key == 'b':
        if action == 'keyDown':
            b = true
        if action == 'keyUp':
            b = false
