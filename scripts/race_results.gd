extends MarginContainer


const MAIN_MENU = preload('res://scenes/menu.tscn')

onready var result_box = $vbox

var results = []


func _ready():
    Network.connect('input', self, '_input_handler')
    Network.send('setController', {
        'vertical': false,
        'modules': [
            {'module': 'TextButton', 'options': {'buttons': ['play again']}},
    ]})
    
    var all_best_times = []
    
    for player in Network.players.values():
        var data = player['data']
        var best_time = data.lap_times.min()
        var total_time = 0
        
        for time in data.lap_times:
            total_time += time
        
        all_best_times.append(best_time)
        results.append([best_time, total_time, data])
    
    
    var best_time = all_best_times.min()
    results.sort_custom(Utils, 'second_element_sort')
    
    for e in Utils.enumerate(results):
        var position
        position.place = e.index + 1
        position.best_time = e.object[0]
        position.total_time = e.object[1]
        position.player_data = e.object[2]
        
        position.top_best_time = e.object[0] == best_time
        
        result_box.add_child(position)


func _input_handler(player, content):
    var action = content['data']['action']
    var key = content['data']['key']
    
    if action == 'keyPressed' and key == 'play again':
        get_tree().change_scene_to(MAIN_MENU)


