extends Node

const VEHICLE_SELECT = preload('res://scenes/vehicle_select.tscn')
const MENU_PLAYER = preload('res://scenes/components/menu_player.tscn')

onready var players_node = $margin/hbox/players

onready var start_button = $margin/hbox/vbox/HBoxContainer/vbox/start
onready var incorrect_player_count = $margin/hbox/vbox/HBoxContainer/vbox/player_count
onready var incorrect_player_count_count = $margin/hbox/vbox/HBoxContainer/vbox/player_count/count
onready var players_not_ready = $margin/hbox/vbox/HBoxContainer/vbox/players_not_ready

var players_ready = {}


func _ready():
    Network.connect('message', self, '_message_handler')
    Network.connect('input', self, '_input_handler')
    Network.connect('ws_ready', self, '_invite_players')
    
    Network.send('setController', {
        'vertical': false,
        'modules': [
            {'module': 'TextButton', 'options': {'buttons': ['start', 'ready']}},
    ]})


func _invite_players():
    while true:
        # request players in the player WS group to resend their information,
        # used to populate the 'players' dict and start RTC connection setup
        Network.wsSend('gameInvite', {})
        print('game invite sent')
        yield(get_tree().create_timer(1), 'timeout')


func _start():
    if not start_button.disabled:
        print('starting')
        get_tree().change_scene_to(VEHICLE_SELECT)


func _set_player_data(node, player):
    var color = Utils.PLAYER_COLORS[player.data.color]
    node.get_node('color').color = Color(color)
    node.get_node('name').text = player.data.name
    node.get_node('status/ready_true').visible = player.data.ready
    node.get_node('status/ready_false').visible = not player.data.ready


func _process(delta):
    var player_nodes = []
    var player_ids = Network.players.keys()
    
    for node in players_node.get_children():
        if node.name in player_ids:
            player_nodes.append(node.name)
            _set_player_data(node, Network.players[node.name])
        else:
            node.queue_free()
    
    for player in Network.players.values():
        if not player.data.id in player_nodes:
            var node = MENU_PLAYER.instance()
            node.name = player.data.id
            _set_player_data(node, player)
            players_node.add_child(node)
    
    var count = 0
    for player in Network.players.values():
        if not player.data.ready:
            count += 1
    
    var player_count = Network.players.size()
    start_button.disabled = count > 0 and player_count > 0 and player_count <= 8
    
    incorrect_player_count.visible = player_count <= 0 or player_count > 8
    incorrect_player_count_count.text = str(player_count)
    players_not_ready.visible = count > 0


func _message_handler(player, content):
    if content['event'] == 'playerJoinedGame':
        print('new player joined')
        player.send('setController', {
            'vertical': false,
            'modules': [
                {'module': 'TextButton', 'options': {'buttons': ['start', 'ready']}},
        ]})


func _input_handler(player, content):
    var action = content['data']['action']
    var key = content['data']['key']
    
    if action == 'keyUp':
        if key == "ready":
            player.data.ready = not player.data.ready
        
        if key == "start":
            _start()
